
declare namespace Api {
  interface Error {
    title: string;
    detail: string;
  }
}