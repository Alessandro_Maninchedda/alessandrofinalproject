import {
  ElementRef,
  ViewContainerRef,
  TemplateRef,
  Directive,
  Input,
  OnInit,
  OnChanges } from '@angular/core';

@Directive({
  selector: '[Highlight]'
})
export class HighlightDirective implements OnInit {

  @Input('Highlight') Highlight;

  constructor(private el: ElementRef) {}

  ngOnInit() {
    this.el.nativeElement.style.backgroundColor = this.Highlight;
  }
}

@Directive({
  selector: '[NgIf]'
})
export class NgIfDirective {

  hasView = false;

  @Input('NgIf') set NgIf(condition: boolean) {
    if (condition && !this.hasView ) {
      this.container.createEmbeddedView(this.template);
      this.hasView = true;
    } else if (!condition && this.hasView ) {
      this.container.clear();
      this.hasView = false;
    }
  }

  constructor(
    private container: ViewContainerRef,
    private template: TemplateRef<any>) {}
}

@Directive({
  selector: '[NgFor]'
})
export class NgForDirective implements OnChanges {

  @Input('NgForOf') NgForOf: Array<any>;

  constructor(
    private container: ViewContainerRef,
    private template: TemplateRef<any>) {}

  ngOnChanges() {
    this.NgForOf.forEach(value => {
      this.container.createEmbeddedView(this.template, { $implicit: value});
    });
  }
}
