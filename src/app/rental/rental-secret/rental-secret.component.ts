import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'rental-secret',
  templateUrl: './rental-secret.component.html',
  styleUrls: ['./rental-secret.component.scss']
})
export class RentalSecretComponent implements OnInit {

  greeting = 'Hello Alessandro!';

  constructor() { }

  ngOnInit() {}

}
